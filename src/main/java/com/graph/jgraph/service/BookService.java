package com.graph.jgraph.service;

import com.graph.jgraph.dao.BookRepository;
import com.graph.jgraph.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {

    @Autowired
    BookRepository bookRepository;

    @Transactional(readOnly = true)
    public List<Book> allBooks(){

        return bookRepository.findAll();
    }


    @Transactional(readOnly = true)
    public Optional<Book> getBook(String id) {
        return bookRepository.findById(id);
    }

}
