package com.graph.jgraph.query;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.graph.jgraph.entity.Book;
import com.graph.jgraph.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class BookQuery implements GraphQLQueryResolver {

    @Autowired
    private BookService bookService;

    public List<Book> getAllBooks() {
        return this.bookService.allBooks();
    }

    public Optional<Book> getBook(final String id) {

        return this.bookService.getBook(id);
    }

}
