package com.graph.jgraph;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import java.util.stream.Stream;

@SpringBootApplication
public class JgraphApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(JgraphApplication.class, args);
	}

}
