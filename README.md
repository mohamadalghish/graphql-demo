# GraphQL Example

## Notes

> 2.6.3 on default, it ran. So it seems like the latest GrahpQL isn't fully compatible with Spring Boot 2.7.0 yet. Which might be obvious because it was released in the last month.

> The final keyword on a method parameter means absolutely nothing to the caller. It also means absolutely nothing to the running program, since its presence or absence doesn't change the bytecode. It only ensures that the compiler will complain if the parameter variable is reassigned within the method. That's all.
## Reference

- [Introduction to Spring GraphQL with Spring Boot](https://www.youtube.com/watch?v=atA2OovQBic&ab_channel=SpringAcademy)
- [GraphQL Java Example for Beginners](https://dzone.com/articles/a-beginners-guide-to-graphql-with-spring-boot)
- [swathisprasad /graphql-with-spring-boot ](https://github.com/swathisprasad/graphql-with-spring-boot)
- [TechPrimers /spring-boot-graphql-query-example](https://github.com/TechPrimers/spring-boot-graphql-query-example)